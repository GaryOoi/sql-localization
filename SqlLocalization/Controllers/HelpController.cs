﻿using SqlLocalization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace SqlLocalization.Controllers
{
    [ServiceFilter(typeof(LanguageActionFilter))]
    [Route("api/{culture}/[controller]")]
    [ApiController]
    public class HelpController : ControllerBase
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        public HelpController(IStringLocalizer<SharedResource> localizer)
        {
            _localizer = localizer;
        }

        [HttpGet]
        public ActionResult<Help> Get()
        {
            return new Help
            {
                HelpTitle = _localizer["HelpTitle"],
                HelpAnswer = _localizer["HelpAnswer"],
            };
        }
    }
}
