﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SqlLocalization
{
    public class Help
    {
        public string HelpTitle { get; set; }

        public string HelpAnswer { get; set; }

    }
}
