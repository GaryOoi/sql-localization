# SQL Localization

 A very simple Web API with .NET Core that return web content with different language selection. 

1. English - https://localhost:5001/api/en-US/help

2. Italian - https://localhost:5001/api/it-CH/help

3. Malay - https://localhost:5001/api/ms-MY/help

4. Chinese - https://localhost:5001/api/zh-CN/help